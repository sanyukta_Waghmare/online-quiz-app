from django import forms
from .models import examdata

class ExamForm(forms.ModelForm):
    class Meta:
        model = examdata
        fields = {'questions','option1','option2','option3','option4','correctans'}

    field_order = ['questions','option1','option2','option3','option4','correctans']